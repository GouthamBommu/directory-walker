import java.util.Iterator;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.io.*;
import java.util.regex.*;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

class FirstAssign
{
	public static boolean bFirstFile = true;
	public static Logger logger = Logger.getLogger(FirstAssign.class.getName());
	public static int iValidRows = 0;
	public static int iSkippedRows = 0;
	
	public static void main(String[] ar)
	{      
		final long startTime = System.currentTimeMillis();
				
		//String sPath = "C:\\Users\\Goutham\\Desktop\\CDA assignments\\Web Development\\New folder";
		String sPath = "C:\\Users\\Goutham\\Desktop\\CDA assignments\\Web Development\\MCDA5510_Assignments\\Sample Data";
		walk(sPath);
		
		final long endTime = System.currentTimeMillis();
		
		writeLog("Valid rows", Integer.toString(iValidRows));	
		writeLog("Skipped rows", Integer.toString(iSkippedRows));
		writeLog("Total execution time", (endTime - startTime) + " ms");		
	}

	public static void walk( String path ) 
	{

        File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;

        for ( File f : list ) 
        {
            if ( f.isDirectory() ) 
            {
                //System.out.println( "Dir:" + f.getAbsoluteFile() );
                walk(f.getAbsoluteFile().toString());
            }
            else 
            {
                //System.out.println( "File:" + f.getAbsoluteFile() );
            	if(f.getAbsoluteFile().toString().contains(".csv"))
            		merge(f.getAbsoluteFile().toString());
            }
        }
    }

	public static void merge(String path)
	{
		Reader in;		
		boolean bFirstRecord = true;
		try 
		{
			///Date//////////////////////
			String sDate = null;
			Pattern patt = Pattern.compile("(\\d+\\\\\\d+\\\\\\d+).*");
    		Matcher mat = patt.matcher(path);
    		if(mat.find())
    			sDate = mat.group(1);
    		
			///////////////////////////
			in = new FileReader(path);						
			FileOutputStream fos = new FileOutputStream("./Output/Final.csv", true);
			PrintWriter pw  = new PrintWriter(fos);
			Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
			String sRecord = "";
			int iRecordLine = 0;
			
			for (CSVRecord record : records) 
			{	
				++iRecordLine;
				if (bFirstRecord && !bFirstFile)
				{
					bFirstRecord = false;
					continue;
				}
				
				boolean bRecordFine = true;			    
				Iterator<String> iter =record.iterator();
				
				while(iter.hasNext())
			    {			    				    	 
			    	String s = iter.next();
			    	sRecord += ("\"" + s + "\""+ ','); 
			    	if(s.isEmpty())
			    	{			    					    		
			    		bRecordFine = false;
			    		iSkippedRows++;
			    		break;
			    	}
			    }
			    		    
			    if(bRecordFine)
			    {		
			    	if(bFirstFile)
			    	{
			    		sRecord += "Date";
			    		bFirstFile = false;
			    	}
			    	else
			    	{
			    		sRecord += sDate;
			    		iValidRows++;
			    	}	    	
			    	pw.println(sRecord);
			    	
			    }
			    else
			    {
			    	writeLog("Skipping record",mat.group(0) + " File - Record " + iRecordLine );
			    }
			    
			    sRecord = "";
			};
			
			pw.close();			
		} 
		catch ( IOException e) 
		{
			writeLog("Exception: ", e.toString());
			e.printStackTrace();			
		}
	}

	public static void writeLog(String sMessage, String sInfo) 
	{		
		try
		{
			Handler fileHandler = null;
			Formatter simpleFormatter = new SimpleFormatter();			
			String sFile = "./logs/sampleLogfile.log";
			File f = new File(sFile);			
			if(! f.exists())
			{
				fileHandler = new FileHandler(sFile);
				fileHandler.setLevel(Level.ALL);
				fileHandler.setFormatter(simpleFormatter);
				logger.addHandler(fileHandler);
			}			
			
			logger.info(sMessage + " : " + sInfo);		
		}
		catch(IOException ex)
		{
			writeLog("Exception: ", ex.toString());
			ex.printStackTrace();		
		}
	}


}